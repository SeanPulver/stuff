package babylonian.algorithm;
import java.util.Scanner;
/**
 *
 * @author Sean Pulver
 */
public class BabylonianAlgorithm {
    public static final double Eternal = 0.01;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
/**
 * This Program is designed to find the square root of any positive integer.
 * We establish a constant variable, I called Eternal.
 * We use the scanner function to input a integer of which we want the square
 * root of. Using a do-while loop we ensure that the integer is a positive
 * integer. After establishing doubles Guess, Ratio, and PreviousGuess, the
 * program then uses the mathematical formula to provide us with an answer.
 * Lastly we us printf to display our answer to include 2 decimal places.
 */
Scanner in = new Scanner(System.in);
int Number = 0;
do
   {
    System.out.println("Please Enter a Positive Integer for the Guess");
    Number = in.nextInt();
         } while (Number < 0);
   
double Guess; 
double PreviousGuess;
           PreviousGuess= Number ;
double Ratio;
   
System.out.println("Guess the square root of " + Number);
  
Guess = in.nextInt();
while ((Math.abs(PreviousGuess - Guess))/Guess >= Eternal) 
{ 
Ratio = (Number / Guess);
PreviousGuess = Guess;
Guess = ((PreviousGuess + Ratio)/2);
System.out.println ("Your guess is " + Guess);
}
 
System.out.printf("%.02f" , Guess);
System.out.println();
    
    }}
